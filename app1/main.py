import sys
from flask import Flask

app = Flask(__name__)

c = 0

@app.route("/")
def hello():
    global c
    if c == 0:
        s = str.format("Hello from container <<{}>>\n", container_name)
    else:
        s = str.format("Hello again ({} times) from container <<{}>>\n", c + 1, container_name)
    c = c + 1
    return s


if __name__ == "__main__":
    container_name = sys.argv[1] if len(sys.argv) > 1 else "empty"
    app.run(host='0.0.0.0')
