import sys
from flask import Flask, session
from flask_session import Session # this line is new

app = Flask(__name__)
# configuration for saving sessions
# the following three lines are also new
SESSION_TYPE = 'filesystem'
app.config.from_object(__name__)
Session(app)

KEY_COUNTER = "counter"

@app.route("/")
def hello():
    if KEY_COUNTER in session:
        c = session[KEY_COUNTER] + 1
        session[KEY_COUNTER] = c
        return str.format("Hello again ({} times) from container <<{}>>\n", c, container_name)
    else:
        session[KEY_COUNTER] = 1
        return str.format("Hello from container <<{}>>\n", container_name)

if __name__ == "__main__":
    container_name = sys.argv[1] if len(sys.argv) > 1 else "empty"
    app.run(host='0.0.0.0')
